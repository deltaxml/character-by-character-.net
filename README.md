# Character By Character Comparison

How character by character comparison can be performed.

This document describes how to run the sample. For concept details see: [Character by Character Comparison](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/character-by-character-comparison)

*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML Compare release. The resources should be located such that they are two levels below the top level release directory.*

*For example `DeltaXML-XML-Compare-10_0_0_n/samples/sample-name`.*

---

## Running the sample
The sample can be run via a run.bat batch file, so long as this is issued from the sample directory. Alternatively, as this batch file contains a single command, the command can be executed directly:

	..\..\bin\deltaxml.exe compare cbc input1.html input2.html output.xml
	
Having produced the comparison result it may be worth adding HTML markup. This can be achieved by processing the result of this command with a simple XSLT 2.0 transformation script *text-groups-to-html.xsl*, which is located in the *extra* subdirectory of this sample.
Note that the HTML markup script is not formally part of the Character By Character sample, but is included to simplify the presentation of the sample results. It converts the deltaxml:textGroup additions and deletions into HTML insertions and deletions respectively, and removes all other DeltaXML markup. It is not intended to be used as a general purpose DeltaV2 to HTML filter, as discussed in the *extra* directory's Read Me document.

# **Note - .NET support has been deprecated as of version 10.0.0 **