<!--
  Copyright (c) 2013 DeltaXML Ltd. All rights reserved.
  
  This converts the character by character results into a sequence of 'delete', 'insert'
  and 'unchanged' elements that contain precisely one of each the original characters, where:
  (1) each element contains some text (a string of one or more characters); and
  (2) the sequence does not contain two consecutive elements of the same type (e.g. 'delete').
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:deltaxml="http://www.deltaxml.com/ns/well-formed-delta-v1"
                version="2.0">

  <xsl:output method="xml" omit-xml-declaration="yes" />

  <xsl:template match="@* | node()" mode="#all">
    <xsl:copy>
      <xsl:apply-templates select="@*, node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <!--
    For any region of text that contains a character change, convert this change into a
    sequence of 'delete', 'insert' and 'unchanged' elements as previously discussed
  -->
  <xsl:template match="*[c]">
    <xsl:copy>
      <xsl:for-each-group select="node()" group-adjacent="deltaxml:groupkey(.)">
        <xsl:variable name="deltaV2" select="current-grouping-key()" />
        <xsl:choose>
          <xsl:when test="$deltaV2='A'">
            <xsl:element name="delete">
              <xsl:apply-templates select="current-group()//text()" />
            </xsl:element>
          </xsl:when>
          <xsl:when test="$deltaV2='B'">
            <xsl:element name="insert">
              <xsl:apply-templates select="current-group()//text()" />
            </xsl:element>
          </xsl:when>
          <xsl:otherwise>
            <xsl:element name="unchanged">
              <xsl:apply-templates select="current-group()//text()" />  
            </xsl:element>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each-group>
    </xsl:copy>
  </xsl:template>
    
  <!--
    The grouping key is the deltaV2 value associated with the given node (character). 
  -->
  <xsl:function name="deltaxml:groupkey" as="xs:string">
    <xsl:param name="node" as="node()" />
    <xsl:choose>
      <xsl:when test="$node/@deltaxml:deltaV2">
        <xsl:value-of select="$node/@deltaxml:deltaV2" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="'A=B'" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
</xsl:stylesheet>